import { Router, DbClient } from './deps.ts'
import { settings, dbClient, logger, sessionContainer, enums, getUserId } from './config.ts'
import * as words from './models/wordsModel.ts'
import * as users from './models/usersModel.ts'

const router     = new Router()
const {programs} = enums

router.get('/search/:search/:id', async (ctx) => {
    if (ctx.params && ctx.params.search && ctx.params.id){
        const result = await words.search(dbClient, ctx.params.search)
        ctx.response.body   = { result, requestId: ctx.params.id }
    }
    else{
        ctx.response.status = 400
    }
})

router.get('/is_logged', async (ctx) => {
    ctx.response.body = JSON.stringify({
        status: sessionContainer.object?.getVar('userId') 
            ? enums.appStatus.logged
            : enums.appStatus.notLogged
    })
})

router.post('/login', async (ctx) => {
    const body = await ctx.request.body()
    let params = await body.value
    params     = JSON.parse(params)
    let user   = null
    if (params.username && params.password){
        user = await users.selectUser(dbClient, params.username, params.password)
        if (user){
            sessionContainer.object?.setVar('userId', user.id)
            ctx.response.body = JSON.stringify({
                status : enums.appStatus.logged
            })
        }
        else{
            ctx.response.body = JSON.stringify({
                status : enums.appStatus.notLogged
            })
        }
    }
    else{
        ctx.response.status = 400 // Bad request
    }

})

router.get('/logout', async (ctx) => {
    sessionContainer.object?.deleteVar('userId')
    ctx.response.body = JSON.stringify({
        status : 0
    })
})

router.get('/get_next_words/:program', async (ctx) => {
    if (ctx.params && ctx.params.program){
        const programId   = Number(ctx.params.program)
        const nextWords   = await words.getNextWords(dbClient, getUserId(), settings.wordsBatch, programId)
        const goodNumber  = await words.getGoodNumber(dbClient, getUserId(), programId)
        ctx.response.body = JSON.stringify({ 
            words : nextWords.rows.map( (row:any) => { 
                return { id: row[0], word: row[1], meaning: row[2] } 
            }),
            stats : {
                total        : goodNumber,
                sessionTotal : sessionContainer.object?.getVar('total') ?? 0,
                sessionGood  : sessionContainer.object?.getVar('good') ?? 0,
            }
        })
    }
    else{
        ctx.response.status = 400
    }
})

router.get('/get_word_details/:id', async (ctx) => {
    if (ctx.params && ctx.params.id){
        const wordId      = Number(ctx.params.id)
        const wordDetails = await words.getWordDetails(dbClient, wordId)
        logger.debug(`[providing details] word id: ${wordId}`)
        ctx.response.body = { examples: wordDetails[0] }
    }
    else{
        ctx.response.status = 400
    }
})

router.get('/get_user_settings', async (ctx) => {
    const goodNumberEtr  = await words.getGoodNumber(dbClient, getUserId(), programs.englishToRussian)
    const goodNumberRte  = await words.getGoodNumber(dbClient, getUserId(), programs.russianToEnglish)
    const goodNumberSetr = await words.getGoodNumber(dbClient, getUserId(), programs.selectEnglishToRussian)
    const goodNumberSrte = await words.getGoodNumber(dbClient, getUserId(), programs.selectRussianToEnglish)
    ctx.response.body = JSON.stringify({
        goodNumbers: {
            etr  : goodNumberEtr,
            rte  : goodNumberRte,
            setr : goodNumberSetr,
            srte : goodNumberSrte
        }
    })
})

router.post('/update_word_priority', async (ctx) => {
    const body = await ctx.request.body()
    let params = await body.value
    params     = JSON.parse(params)
    let good   = true

    const { wrongs, wordId, program} = params
    const wordSortData               = await words.getWordSortData(dbClient, wordId, getUserId(), program)
    let [ sortId, priority, pushFactor ] = wordSortData.rows[0]

    switch (wrongs) {
        case 0 :
            priority   += pushFactor
            pushFactor *= settings.factorMultiplier
            words.upgradeGood(dbClient, wordId, getUserId(), program)
        break;
        case 1 :
            priority += pushFactor * settings.pushDiminisher
            words.upgradeNotSoGood(dbClient, wordId, getUserId(), program)
        break;
        case 2 :
            priority   += pushFactor * settings.pushDiminisher / 2
            pushFactor *= settings.factorDiminisher
            words.upgradeBad(dbClient, wordId, getUserId(), program)
            good = false
        break;
    }
    priority   = Math.round(priority)
    pushFactor = Math.round(pushFactor)

    const updateResult = await words.updatePriority(dbClient, priority, pushFactor, sortId, program)

    logger.debug( `[updating] user id: ${getUserId()}, word id: ${wordId}, push factor ${pushFactor}, priority: ${priority}` )
    addOne('total')
    if (good){
        addOne('good')
    }

    ctx.response.body = { status: 0 }
})

router.get('/get_next_selection_words/:program', async (ctx) => {
    let program = 0
    if (ctx.params && ctx.params.program){
        program = Number(ctx.params.program)
    }
    else{
        ctx.response.status = 400
        return
    }
    const [ wordId, wordRussian, typeId, meaningEnglish ] = await words.getOneWord(dbClient, getUserId(), program)
    let maybeOptions = await words.getRandomWords(dbClient,typeId,wordId,program)
    const goodNumber = await words.getGoodNumber(dbClient,getUserId(),program)
    const rand = Math.floor(Math.random() * 10)
    let options:Array<any> = maybeOptions
    let word, meaning
    if (program == programs.selectEnglishToRussian){
        word    = wordRussian,
        meaning = meaningEnglish
    }
    else{
        word    = meaningEnglish,
        meaning = wordRussian
    }
    options.splice(rand, 0, [ wordId, word ])
    ctx.response.body = JSON.stringify({
        status  : 0,
        word    : { id: wordId, word: meaning },
        options : options.map( (option:any) => {
            let [ id, word ] = option
            return { id, word, state: "clean" }
        }),
        stats : {
            total        : goodNumber,
            sessionTotal : sessionContainer.object?.getVar('total') ?? 0,
            sessionGood  : sessionContainer.object?.getVar('good') ?? 0,
        }
    })
})

function addOne(varName: string){
    let varValue = sessionContainer.object?.getVar(varName)
    if (varValue == null){
        sessionContainer.object?.setVar(varName, 1)
    }
    else{
        sessionContainer.object?.setVar(varName, varValue + 1)
    }
}

export default router
