import { DbClient, log } from './deps.ts'
import { Session } from './session.ts'

let sessionContainer : { object : Session | null } = {
    object : null
}

const dbClient = new DbClient({
    user        : "rusov",
    database    : "rusov",
    hostname    : "localhost",
    port        : 5432
})

await dbClient.connect()

await log.setup({
    handlers: {
        console: new log.handlers.ConsoleHandler("INFO"),
        file: new log.handlers.FileHandler("DEBUG", {
            filename: "./logs/rusov.log",
            formatter: lr => { return `${lr.datetime.toISOString()} ${lr.levelName} ${lr.msg}`}
        })
    },
    loggers: {
        default: {
            level: "DEBUG",
            handlers: ["console","file"]
        }
    }
})

export { dbClient }

const settings = {
    wordsBatch          : 10,
    factorMultiplier    : 1.6,
    factorDiminisher    : 0.6,
    pushDiminisher      : 0.2,
    userId              : 1,
    appUrl              : 'localhost:3000'
}

const enums = {
    programs : {
        englishToRussian : 1,
        russianToEnglish : 2,
        selectEnglishToRussian : 3,
        selectRussianToEnglish : 4
    },
    appStatus : {
        notLogged : {
            code : 10,
            text : "User not logged"
        },
        logged : {
            code : 20,
            text : "User logged"
        },
        unexpectedError : {
            code : 30,
            text : "there was an unexpected error"
        }
    }
}

export function getUserId():number {
    return sessionContainer.object?.getVar('userId')
}

const logger = log.getLogger()

export { settings, logger, sessionContainer, enums }
