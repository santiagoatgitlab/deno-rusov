const queries = {
    get_next_words           : await Deno.readTextFile('./queries/get_next_words.sql'),
    update_priority          : await Deno.readTextFile('./queries/update_priority.sql'),
    get_word_sort_data       : await Deno.readTextFile('./queries/get_word_sort_data.sql'),
    update_good_count        : await Deno.readTextFile('./queries/update_good_count.sql'),
    update_not_so_good_count : await Deno.readTextFile('./queries/update_not_so_good_count.sql'),
    update_bad_count         : await Deno.readTextFile('./queries/update_bad_count.sql'),
    add_words_count_record   : await Deno.readTextFile('./queries/add_words_count_record.sql'),
    get_word_details         : await Deno.readTextFile('./queries/get_word_details.sql'),
    get_good_number          : await Deno.readTextFile('./queries/get_good_number.sql'),
    search                   : await Deno.readTextFile('./queries/search.sql'),
    select_user              : await Deno.readTextFile('./queries/select_user.sql'),
    get_one_word             : await Deno.readTextFile('./queries/get_one_word.sql'),
    select_random_russian    : await Deno.readTextFile('./queries/select_random_russian.sql'),
    select_random_trans      : await Deno.readTextFile('./queries/select_random_trans.sql')
}

export default queries
