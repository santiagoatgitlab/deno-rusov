update users.words_sort
set 
    priority = $1,
    push_factor = $2
where id = $3;
