update stats.words_count 
set good = good + 1
where word_id = $1
  and user_id = $2
  and program_id = $3
