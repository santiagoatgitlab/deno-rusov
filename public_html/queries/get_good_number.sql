select max(word_id) as total
from stats.words_count
where good > 0
  and user_id = $1
  and program_id = $2
