select
  id,
  priority,
  push_factor
from users.words_sort
where word_id = $1
  and user_id = $2
  and program_id = $3
