SELECT w.id, t.translation as word
FROM words.words w
JOIN words.translations t 
    ON t.word_id = w.id
WHERE w.type_id = $1
  AND w.id != $2
ORDER BY random()
limit 9
