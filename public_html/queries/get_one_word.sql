SELECT 
	w.id,
	w.word,
    w.type_id,
	array_to_json(array_agg(t.translation)) as meaning
FROM words.words w
JOIN users.words_sort ws ON w.id = ws.word_id
JOIN words.translations t ON w.id = t.word_id
WHERE w.type_id is not null
  AND ws.user_id = $1
  AND ws.program_id = $2
GROUP BY 
	w.id,
	ws.priority
ORDER BY ws.priority,w.id
limit 1;
