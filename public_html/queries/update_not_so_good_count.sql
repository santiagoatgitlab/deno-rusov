update stats.words_count 
set not_so_good = not_so_good + 1
where word_id = $1
  and user_id = $2
  and program_id = $3
