select word_id, translation
from (
  select t.word_id, string_agg(t.translation, ' - ') as translation
  from words.translations t
  left join users.words_sort ws on ws.word_id = t.word_id
  where ws.program_id = 2 -- russian to english!
  group by (t.word_id, ws.priority)
  order by ws.priority
  limit 1000
) as next_words
where translation like $1
order by LENGTH(translation)
