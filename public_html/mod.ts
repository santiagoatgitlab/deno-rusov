import { Application, FileHandler, Env } from './deps.ts'
import { logger, sessionContainer, enums } from './config.ts'
import { Session } from './session.ts'
import router from './routes.ts'

const port = 7000
const app  = new Application()
const env  = new Env()

const fileHandler = <FileHandler> logger.handlers[1]

app.use(async (ctx, next) => {
    try{
        sessionContainer.object = new Session(ctx.cookies)
        await next()
        if (!sessionContainer.object?.cookieSet){
            ctx.cookies.set("sessionId", String(sessionContainer.object?.id))
        }
    }
    catch(e){
        console.log(e)
        throw e
    }
})

app.use(async (ctx, next) => {
    ctx.response.headers.set('Access-Control-Allow-Origin', 'http://localhost:3000')
    ctx.response.headers.set('Access-Control-Allow-Credentials', 'true')
    await next()
})

app.use(async (ctx, next) => {
    await next()
    fileHandler.flush()
})

app.use(async (ctx, next) => {
    let userId = sessionContainer.object?.getVar('userId')

    let notLoggedNorLogging = !userId &&
        !(urlTail => ['login','is_logged'].some(word => urlTail == word))
        (ctx.request.url.toString().split('/').reverse()[0])

    if (notLoggedNorLogging){
        ctx.response.body = JSON.stringify({
            status : enums.appStatus.notLogged
        })
        ctx.response.status = 401 // Unauthorized
    }
    else{
        await next()
    }
})

app.use(async (ctx, next) => {
    try{
        await next()
    }
    catch(e){
        ctx.response.status = 500
        logger.error(e.message)
    }
})

app.use(router.routes())
app.use(router.allowedMethods())

logger.info(`[putting up server] port: ${port}`)
fileHandler.flush()
app.listen({ port })
