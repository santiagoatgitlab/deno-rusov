import queries from '../queries.ts'
import { DbClient} from '../deps.ts'
import { enums, logger } from '../config.ts'

const {programs} = enums

export async function getGoodNumber(dbClient:DbClient, userId:number, program:number){
    let result = await dbClient.queryArray({
        text: queries.get_good_number,
        args: [ userId, program ]
    })
    return Number(result.rows[0][0])
}

export async function search(dbClient:DbClient, search:string){
    let result = await dbClient.queryArray({
        text : queries.search,
        args : [ `%${search}%` ]
    })
    return result.rows.map(([id, meaning], pos) => {
        return { pos, id, meaning}
    })
}

export async function getNextWords(dbClient:DbClient, userId:number, limit:number, program:number){
    let result = await dbClient.queryArray({
        text: queries.get_next_words,
        args: [ userId, program, limit ]
    })
    return result
}

export async function upgradeGood(dbClient:DbClient, wordId:number, userId:number, program:number){
    let result = await dbClient.queryArray({
        text: queries.update_good_count,
        args: [ wordId, userId, program ]
    })
    if (result.rowCount == 0){
        result = await dbClient.queryArray({
            text: queries.add_words_count_record,
            args: [ wordId, userId, program, 1, 0, 0 ]
        })
    }
    return result
}

export async function upgradeNotSoGood(dbClient:DbClient, wordId:number, userId:number, program:number){
    let result = await dbClient.queryArray({
        text: queries.update_not_so_good_count,
        args: [ wordId, userId, program ]
    })
    if (result.rowCount == 0){
        result = await dbClient.queryArray({
            text: queries.add_words_count_record,
            args: [ wordId, userId, program, 0, 1, 0 ]
        })
    }
    return result
}

export async function upgradeBad(dbClient:DbClient, wordId:number, userId:number, program:number){
    let result = await dbClient.queryArray({
        text: queries.update_bad_count,
        args: [ wordId, userId, program ]
    })
    if (result.rowCount == 0){
        result = await dbClient.queryArray({
            text: queries.add_words_count_record,
            args: [ wordId, userId, program, 0, 0, 1 ]
        })
    }
    return result
}

export async function getWordSortData(dbClient:DbClient, wordId:number, userId:number, program:number){
    let result = dbClient.queryArray({
        text: queries.get_word_sort_data,
        args: [ wordId, userId, program ]
    })
    return result
}

export async function updatePriority(dbClient:DbClient, priority:number, pushFactor:number, id:number, program:number){
    let result = await dbClient.queryArray({
        text : queries.update_priority,
        args : [ priority, pushFactor, id ]
    })
    return result
}

export async function getWordDetails(dbClient:DbClient, wordId:number){
    let result = await dbClient.queryArray({
        text: queries.get_word_details,
        args: [ wordId ]
    })
    return result.rows[0]
}

export async function getOneWord(dbClient:DbClient, userId:number, program:number){
    let result = await dbClient.queryArray({
        text: queries.get_one_word,
        args: [ userId, program ]
    })
    return result.rows[0]
}

export async function getRandomWords(dbClient:DbClient, typeId:number, wordId:number, program:number){
    let result = await dbClient.queryArray({
        text: program == programs.selectEnglishToRussian
            ? queries.select_random_russian
            : queries.select_random_trans,
        args: [ typeId, wordId ]
    })
    return result.rows
}
