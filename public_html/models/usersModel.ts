import queries from '../queries.ts'
import { DbClient, Sha256 } from '../deps.ts'
import { logger } from '../config.ts'

export async function selectUser(dbClient:DbClient, username:string, password:string){

    const sha256 = new Sha256()

    logger.info(`username model ${username}`)
    logger.info(`password model ${password}`)
    logger.info(`password model sha ${sha256.update(password).hex()}`)
    const result = await dbClient.queryArray({
       text : queries.select_user,
       args : [ username, sha256.update(password).hex() ]
    })
    const user = result.rows.map( row => {
        const [ id, firstName, lastName ] = row
        return { id, firstName, lastName }
    })
    return user.length > 0 ? user[0] : null
}
