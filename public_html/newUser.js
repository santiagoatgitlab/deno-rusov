import { dbClient } from './config.ts'

let insertQuery = 'insert into users.words_sort (word_id, user_id, priority, push_factor)'
insertQuery    += ' values ($1, $2, $3, $4)'

let insertWords = (wordsIds) => {
    wordsIds.forEach( wordId => {
        dbClient.query({
            text: insertQuery,
            args: [ wordId, 3, wordId, 10 ]
        }).then( result => {
            console.log(result)
        }).catch( e => {
            console.log(e)
        })
    })
}

dbClient.query({
    text : "select word_id from stats.words_count where user_id = 1 order by word_id limit 10",
    args : []
}).then( result => {
    let ids = result.rows.map( row => row[0] )
    insertWords(ids)
}).catch( e => {
    console.log(e)
})
