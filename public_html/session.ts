import { DB           } from "https://deno.land/x/sqlite/mod.ts"
import { Cookies      } from "https://deno.land/x/oak@v6.3.1/cookies.ts"
import { uniqueString } from "https://deno.land/x/unique-string@1.0.0/index.js"

const db = new DB("deno-sessions")
db.query('drop table if exists sessionIds');
db.query('drop table if exists vars');
db.query('create table sessionIds (sessionId varchar (64), lastUpdate varchar(13));');
db.query('create unique index uniqueSessionId on sessionIds (sessionId)');
db.query('create table vars (sessionId varchar (64), name varchar(13), value string(255));');
db.query('create unique index uniqueSessionValue on vars (sessionId, name)');

class Session {
    public id : string | undefined
    public cookieSet = true
    private vars = new Map()

    constructor(cookies: Cookies){
        this.id = cookies.get('sessionId')
        if  (!this.id){
            this.id         = uniqueString(64)
            this.cookieSet  = false
            const rowId     = this.createInDatabase()
            if (rowId == 0){
                throw new Error("SessionManager: there was an error while attempting to create a new session")
            }
        }
        else{
            const rows = db.query('select name, value from vars where sessionId = (?)', [ this.id ])
            for (let [ name, value ] of rows){
                value = !isNaN(value) ? Number(value) : value
                this.vars.set(name, value)
            }
        }
    }

    private createInDatabase(){
        let newSession = false
        let rows = db.query("select rowid,sessionId from sessionIds where sessionId = (?)", [this.id])
        if (rows.length == 0){
            rows = db.query("insert into sessionIds values ((?), strftime('%Y-%m-%d %H:%M:%S.$f'))", [this.id])
        }
        return db.lastInsertRowId
    }

    public setVar(name: string, value: any){
        const selectRes = db.query('select * from vars where sessionId = (?) and name = (?)', [ this.id, name ])
        if (selectRes.length == 0){
            db.query('insert into vars values ((?), (?), (?))', [ this.id, name, value ])
        }
        else{
            db.query('update vars set value = (?) where sessionId = (?) and name = (?)', [ value, this.id, name ])
        }
        this.vars.set(name, value)
    }

    public getVar(name: string){
        return this.vars.get(name)
    }

    public deleteVar(name: string){
        db.query('delete from vars where sessionId = (?) and name = (?)', [ this.id, name ])
        this.vars.delete(name)
    }
}

export { Session }
