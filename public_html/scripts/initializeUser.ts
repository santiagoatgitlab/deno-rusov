import { DbClient } from '../deps.ts'

const user_id = 1
const program_id = 4

const dbClient = new DbClient({
    user        : "rusov",
    database    : "rusov",
    hostname    : "localhost",
    port        : 5432
})

await dbClient.connect()

dbClient.query({
    text: "select id from words.words order by id",
    args: []
})
.then( result => {
    result.rows.slice(10).forEach( row => {
        let [word_id] = row
        let query = `insert into users.words_sort (user_id, word_id, program_id, priority) values ($1,$2,$3,$4)`
        dbClient.query({
            text: query,
            args: [user_id, word_id, program_id, word_id]
        })
    })
})
