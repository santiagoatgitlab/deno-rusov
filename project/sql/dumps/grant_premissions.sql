--
-- Name: TABLE aspect_match; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.aspect_match TO rusov;


--
-- Name: TABLE cases; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.cases TO rusov;


--
-- Name: SEQUENCE cases_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.cases_id_seq TO rusov;


--
-- Name: TABLE conjugations; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.conjugations TO rusov;


--
-- Name: SEQUENCE conjugations_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.conjugations_id_seq TO rusov;


--
-- Name: TABLE declinations; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.declinations TO rusov;


--
-- Name: SEQUENCE declinations_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.declinations_id_seq TO rusov;


--
-- Name: TABLE examples; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.examples TO rusov;


--
-- Name: SEQUENCE examples_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.examples_id_seq TO rusov;


--
-- Name: TABLE extra; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.extra TO rusov;


--
-- Name: SEQUENCE extra_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.extra_id_seq TO rusov;


--
-- Name: TABLE genders; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.genders TO rusov;


--
-- Name: SEQUENCE genders_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.genders_id_seq TO rusov;


--
-- Name: TABLE languages; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.languages TO rusov;


--
-- Name: SEQUENCE languages_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.languages_id_seq TO rusov;


--
-- Name: TABLE tags; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.tags TO rusov;


--
-- Name: SEQUENCE tags_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.tags_id_seq TO rusov;


--
-- Name: TABLE tags_words; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.tags_words TO rusov;


--
-- Name: TABLE tenses; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.tenses TO rusov;


--
-- Name: SEQUENCE tenses_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.tenses_id_seq TO rusov;


--
-- Name: TABLE translations; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.translations TO rusov;


--
-- Name: SEQUENCE translations_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.translations_id_seq TO rusov;


--
-- Name: TABLE types; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.types TO rusov;


--
-- Name: SEQUENCE types_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.types_id_seq TO rusov;


--
-- Name: TABLE words; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.words TO rusov;


--
-- Name: SEQUENCE words_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.words_id_seq TO rusov;


--
-- PostgreSQL database dump complete
--


