CREATE TABLE stats.words_count_rte(
    word_id integer not null,
    user_id integer not null,
    good integer not null default 0,
    not_so_good integer not null default 0,
    bad integer not null default 0
);
ALTER TABLE stats.words_count_rte
ADD CONSTRAINT count_user_rte FOREIGN KEY (user_id) REFERENCES users.users (id);
ALTER TABLE stats.words_count_rte
ADD CONSTRAINT count_word_rte FOREIGN KEY (word_id) REFERENCES words.words (id);
alter table stats.words_count_rte
add constraint uniqueness_rte unique (word_id, user_id);
