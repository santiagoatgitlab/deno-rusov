CREATE TABLE stats.words_count(
    word_id integer not null,
    user_id integer not null,
    program_id integer not null,
    good integer not null default 0,
    not_so_good integer not null default 0,
    bad integer not null default 0,
);
ALTER TABLE stats.words_count
ADD CONSTRAINT count_user FOREIGN KEY (user_id) REFERENCES users.users (id);
ALTER TABLE stats.words_count
ADD CONSTRAINT count_word FOREIGN KEY (word_id) REFERENCES words.words (id);
ALTER TABLE stats.words_count
ADD CONSTRAINT count_program FOREIGN KEY (program_id) REFERENCES rusov.programs (id);
ALTER TABLE stats.words_count
ADD constraint uniqueness unique (word_id, user_id, program_id);
