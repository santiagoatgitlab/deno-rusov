CREATE TABLE users.users(
    id          serial       PRIMARY KEY,
    first_name  varchar(128) NOT NULL,
    last_name   varchar(128) NOT NULL,
    email       varchar(128) NOT NULL UNIQUE,
    password    varchar(128) NOT NULL,
    created_at  timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP
);
