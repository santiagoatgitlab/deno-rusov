CREATE TABLE words.tenses(
    id              serial         PRIMARY KEY,
    denomination    varchar(128)    NOT NULL
);
