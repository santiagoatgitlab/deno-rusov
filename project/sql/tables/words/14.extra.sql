CREATE TABLE words.extra(
    id                      serial         PRIMARY KEY,
    word_id                 integer        NOT NULL,
    extra                   varchar(128)   NOT NULL
);
