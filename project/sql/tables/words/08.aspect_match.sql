CREATE TABLE words.aspect_match(
    perfective_word_id     integer,
    imperfective_word_id   integer,
    constraint uniqueness unique (perfective_word_id, imperfective_word_id);
);
