CREATE TABLE words.types(
    id              serial         PRIMARY KEY,
    denomination    varchar(128)    NOT NULL
);
