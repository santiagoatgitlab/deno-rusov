ALTER TABLE words.tags_words
ADD CONSTRAINT tag_word_word FOREIGN KEY (word_id) REFERENCES words (id);
ALTER TABLE words.tags_words
ADD CONSTRAINT tag_word_tag FOREIGN KEY (tag_id) REFERENCES tags (id);
