CREATE TABLE words.pronouns_person(
    word_id     integer NOT NULL,
    person      smallint NOT NULL,
    number      varchar(1) NOT NULL
);
