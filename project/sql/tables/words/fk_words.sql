ALTER TABLE words
ADD CONSTRAINT words_type_fk FOREIGN KEY (type_id) REFERENCES types (id);
ALTER TABLE words
ADD CONSTRAINT words_gender_fk FOREIGN KEY (gender_id) REFERENCES genders (id);
