CREATE TABLE words.words(
    id          serial         PRIMARY KEY,
    word        varchar(128)    NOT NULL UNIQUE,
    type_id     integer,
    gender_id   integer
);
