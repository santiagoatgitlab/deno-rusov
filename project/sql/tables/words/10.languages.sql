CREATE TABLE words.languages(
    id              serial          PRIMARY KEY,
    denomination    varchar(128)
)
