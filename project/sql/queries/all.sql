select 
    w.id,
    w.word,
    t.denomination as type,
    g.denomination as gender,
    substring(m.translation,0,25) as meaning,
    e.extra
from words.words w 
left join words.types t on w.type_id = t.id
left join words.genders g on w.gender_id = g.id
left join words.translations m on w.id = m.word_id
left join words.extra e on w.id = e.word_id
