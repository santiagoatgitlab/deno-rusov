select w.id, w.word, t.denomination 
from words.words w 
join words.types t
    on t.id = w.type_id
order by id;
