select t.denomination, count(w.id) as total
from words.words w 
join words.types t
    on t.id = w.type_id
group by t.denomination
order by total;
