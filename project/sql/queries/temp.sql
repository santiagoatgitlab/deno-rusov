select w.word,t.denomination
from words.words w 
left join words.types t
    on t.id = w.type_id
where w.id < 3000
and w.type_id is null
order by w.id;
