select w.word, t.denomination, tr.translation
from words.tags_words tw
left join words.words w 
    on w.id = tw.word_id
left join words.tags t 
    on t.id = tw.tag_id
left join words.translations tr
    on tr.word_id = w.id
order by t.id;
