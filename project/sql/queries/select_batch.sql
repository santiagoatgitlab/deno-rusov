select w.id, w.word, array_agg(t.translation) as meaning, ws.urgent, ws.priority
from words.words w
join users.words_sort ws
    on w.id = ws.word_id
join words.translations t
    on w.id = t.word_id
where ws.user_id = 1
group by w.id, ws.urgent, ws,priority
order by ws.urgent desc, ws.priority desc, w.id

limit 20;
